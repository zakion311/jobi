let nav = document.getElementById("navigation"),
            corp = document.getElementById("corp"),
            navWidth = nav.offsetWidth;
            console.log(navWidth)
            corp.style.marginLeft = navWidth+"px"
            corp.style.marginRight = navWidth+"px"
            corp.style.width = (corp.offsetWidth - navWidth)+ "px";

        let blockTwoBorder = document.getElementById('blockTwoBorder'),
            blockOneBorder = document.getElementById('blockOneBorder'),
            blockThreeBorder = document.getElementById('blockThreeBorder'),
            blockTwo = document.getElementById('blockTwo'),
            blockOne = document.getElementById('blockOne'),
            blockThree = document.getElementById('blockThree'),
            dropDetailsBlockOne = document.getElementById('dropDetailsBlockOne'),
            dropModifierBlockOne = document.getElementById('dropModifierBlockOne2'),
            dropSupprimerBlockOne = document.getElementById('dropSupprimerBlockOne'),
            dropModifierBlockTwo = document.getElementById('dropModifierBlockTwo'),
            dropSupprimerBlockTwo = document.getElementById('dropSupprimerBlockTwo'),
            dropDetailsBlockThree = document.getElementById('dropDetailsBlockThree'),
            dropModifierBlockThree = document.getElementById('dropModifierBlockThree'),
            dropSupprimerBlockThree = document.getElementById('dropSupprimerBlockThree');
            

            blockOne.addEventListener('mouseover', () => {
                blockOneBorder.className=" border border-primary w-100 mt-1 mb-3"
            });
            blockOne.addEventListener('mouseout', () => {
                blockOneBorder.className="border w-100 mt-1 mb-3"
            });

            blockTwo.addEventListener('mouseover', () => {
                blockTwoBorder.className=" border border-primary w-100 mt-1 mb-3"
            });
            blockTwo.addEventListener('mouseout', () => {
                blockTwoBorder.className="border w-100 mt-1 mb-3"
            });

            blockThree.addEventListener('mouseover', () => {
                blockThreeBorder.className=" border border-primary w-100 mt-1 mb-3"
            });
            blockThree.addEventListener('mouseout', () => {
                blockThreeBorder.className="border w-100 mt-1 mb-3"
            });
            
            dropModifierBlockOne.addEventListener('mouseover', () => {
                dropModifierBlockOne.className = "dropdown-item active"
            })
            dropModifierBlockOne.addEventListener('mouseout', () => {
                dropModifierBlockOne.className="dropdown-item"
            })
            dropSupprimerBlockOne.addEventListener('mouseover', () => {
                dropSupprimerBlockOne.className = "dropdown-item bg-danger text-white"
            })
            dropSupprimerBlockOne.addEventListener('mouseout' , () => {
                dropSupprimerBlockOne.className="dropdown-item"
            })

            dropDetailsBlockTwo.addEventListener('mouseover', () => {
                dropDetailsBlockTwo.className = "dropdown-item active";
            })
            dropDetailsBlockTwo.addEventListener('mouseout', () => {
                dropDetailsBlockTwo.className="dropdown-item";
            })
            dropModifierBlockTwo.addEventListener('mouseover', () => {
                dropModifierBlockTwo.className="dropdown-item active";
            })
            dropModifierBlockTwo.addEventListener('mouseout', () => {
                dropModifierBlockTwo.className="dropdown-item";
            })
            dropSupprimerBlockTwo.addEventListener('mouseover', () => {
                dropSupprimerBlockTwo.className="dropdown-item bg-danger text-white";
            })
            dropSupprimerBlockTwo.addEventListener('mouseout', () => {
                dropSupprimerBlockTwo.className="dropdown-item"
            })


            dropDetailsBlockThree.addEventListener('mouseover', () => {
                dropDetailsBlockThree.className = "dropdown-item active";
            })
            dropDetailsBlockThree.addEventListener('mouseout', () => {
                dropDetailsBlockThree.className="dropdown-item";
            })
            dropModifierBlockThree.addEventListener('mouseover', () => {
                dropModifierBlockThree.className="dropdown-item active";
            })
            dropModifierBlockThree.addEventListener('mouseout', () => {
                dropModifierBlockThree.className="dropdown-item";
            })
            dropSupprimerBlockThree.addEventListener('mouseover', () => {
                dropSupprimerBlockThree.className="dropdown-item bg-danger text-white";
            })
            dropSupprimerBlockThree.addEventListener('mouseout', () => {
                dropSupprimerBlockThree.className="dropdown-item"
            })
           
