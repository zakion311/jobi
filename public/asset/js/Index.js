let nav = document.getElementById("navigation"),
            corp = document.getElementById("corp"),
            navWidth = nav.offsetWidth;
            console.log(navWidth)
            corp.style.marginLeft = navWidth+"px"
            corp.style.marginRight = navWidth+"px"
            corp.style.width = (corp.offsetWidth - navWidth)+ "px";

let cartes = document.getElementsByClassName("carte"),
        border = document.getElementById("border"),
        fond = document.getElementById("fond"),
        addButton = document.getElementById("addButton");

        fond.addEventListener("mouseover" , () => {
            border.className = "border-top border border-primary  w-75 mt-1 mb-3";
        });

        fond.addEventListener("mouseout" , () => {
            border.className = "border-top border w-75 mt-1 mb-3";
        });

        addButton.addEventListener("mouseover", ()=>{
            addButton.className="container-fluid border zindex-popover shadow-lg p-2 mr-3 h-100 table-head-color text-center";
        });

        addButton.addEventListener("mouseout", ()=>{
            addButton.className="container-fluid border opacity-sm shadow-sm p-2 mr-3 h-100 table-head-color text-center";
        })

        for (let i = 0; i < cartes.length; i++) {
            cartes[i].addEventListener("mouseover", () => {
                cartes[i].className = "carte container-fluid bg-white shadow-lg zindex-popover p-2 mr-3 border"; 
            });
            cartes[i].addEventListener("mouseout", () => {
                cartes[i].className="carte container-fluid bg-white shadow-sm p-2 mr-3 border";
            })
        }
        
      
       