let nav = document.getElementById("navigation"),
            corp = document.getElementById("corp"),
            navWidth = nav.offsetWidth;
            console.log(navWidth)
            corp.style.marginLeft = navWidth+"px"
            corp.style.marginRight = navWidth+"px"
            corp.style.width = (corp.offsetWidth - navWidth)+ "px";

let borderMain = document.getElementById('borderMain'),
    fond = document.getElementById('fond'),
    societe = document.getElementById('societe'),
    details = document.getElementById('details'), 
    borderOne = document.getElementById('borderOne'),
    borderTwo = document.getElementById('borderTwo');

    
fond.addEventListener('mouseover', () =>{
    borderMain.className = "border-top border border-primary w-75 mt-1 mb-3";
});

fond.addEventListener('mouseout', ()=>{
    borderMain.className = "border-top border w-75 mt-1 mb-3";
});

societe.addEventListener('mouseover', () => {
    borderOne.className = "border-top border border-primary w-75 mt-1 mb-3";
    societe.className = "container-fluid col-5 h-50 p-2 pb-4 mb-2 border zindex-popover shadow"
});

societe.addEventListener('mouseout', () => {
    borderOne.className = "border-top border w-75 mt-1 mb-3";
    societe.className = "container-fluid col-5 h-50 p-2 pb-4 mb-2 border shadow-sm"
});

details.addEventListener('mouseover', () => {
    borderTwo.className="border-top border border-primary w-75 mt-1 mb-3";
    details.className="container-fluid col-5 h-50 p-2 pb-4 mb-2 border zindex-popover shadow";
});

details.addEventListener('mouseout', () => {
    borderTwo.className="border-top border w-75 mt-1 mb-3";
    details.className="container-fluid col-5 h-50 p-2 pb-4 mb-2 border shadow-sm"
});

let starOne = document.getElementById('starOne'),
    starTwo = document.getElementById('starTwo'),
    starThree = document.getElementById('starThree'),
    starFour = document.getElementById('starFour'),
    starFive = document.getElementById('starFive'),
    starOneClick = false,
    starTwoClick = false,
    starThreeClick = false,
    starFourClick = false,
    starFiveClick = false;

starOne.addEventListener('mouseover', () => {
    starOne.innerHTML="star";
    
    
});
starOne.addEventListener('mouseout', () => {
    if(!starOneClick && !starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starOne.innerHTML="star_border";
    }
    
});
starOne.addEventListener('click', () => {
    starOneClick = true;
    starTwoClick = false;
    starThreeClick = false;
    starFourClick = false;
    starFiveClick = false;
    starOne.innerHTML="star";
    starTwo.innerHTML="star_border";
    starThree.innerHTML="star_border";
    starFour.innerHTML="star_border";
    starFive.innerHTML="star_border";
});

starTwo.addEventListener('mouseover', () => {
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    
    
});
starTwo.addEventListener('mouseout', () => {
    if(!starOneClick && !starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starOne.innerHTML="star_border";
        starTwo.innerHTML="star_border";
    }
    else if(starOneClick && !starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starTwo.innerHTML="star_border";
    }
    
});
starTwo.addEventListener('click', () => {
    starOneClick = false;
    starTwoClick = true;
    starThreeClick = false;
    starFourClick = false;
    starFiveClick = false;
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star_border";
    starFour.innerHTML="star_border";
    starFive.innerHTML="star_border";
});
starThree.addEventListener('mouseover', () => {
    
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star";
    
    
});
starThree.addEventListener('mouseout', () => {
    if(!starOneClick && !starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starOne.innerHTML="star_border";
        starTwo.innerHTML="star_border";
        starThree.innerHTML="star_border";
    }
    else if (!starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starTwo.innerHTML="star_border";
        starThree.innerHTML="star_border";
    }
    else if(!starThreeClick && !starFourClick && !starFiveClick){
        starThree.innerHTML="star_border";
    }
    
    
});
starThree.addEventListener('click', () => {
    starOneClick = false;
    starTwoClick = false;
    starThreeClick = true;
    starFourClick = false;
    starFiveClick = false;
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star";
    starFour.innerHTML="star_border";
    starFive.innerHTML="star_border";
});
starFour.addEventListener('mouseover', () => {
    
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star";
    starFour.innerHTML="star";
   
    
});
starFour.addEventListener('mouseout', () => {
    if(!starOneClick && !starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starOne.innerHTML="star_border";
        starTwo.innerHTML="star_border";
        starThree.innerHTML="star_border";
        starFour.innerHTML="star_border";
    }
    else if(!starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starTwo.innerHTML="star_border";
        starThree.innerHTML="star_border";
        starFour.innerHTML="star_border";
    }
    else if (!starThreeClick && !starFourClick && !starFiveClick){
        starThree.innerHTML="star_border";
        starFour.innerHTML="star_border";
    }
    else if(!starFourClick && !starFiveClick){
        starFour.innerHTML="star_border";
    }
    
});
starFour.addEventListener('click', () => {
    starOneClick = false;
    starTwoClick = false;
    starThreeClick = false;
    starFourClick = true;
    starFiveClick = false;
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star";
    starFour.innerHTML="star";
    starFive.innerHTML="star_border";
})
starFive.addEventListener('mouseover', () => {
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star";
    starFour.innerHTML="star";
    starFive.innerHTML="star";

    
});
starFive.addEventListener('mouseout', () => {
    if(!starOneClick && !starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starOne.innerHTML="star_border";
        starTwo.innerHTML="star_border";
        starThree.innerHTML="star_border";
        starFour.innerHTML="star_border";
        starFive.innerHTML="star_border";
    }
    else if(!starTwoClick && !starThreeClick && !starFourClick && !starFiveClick){
        starTwo.innerHTML="star_border";
        starThree.innerHTML="star_border";
        starFour.innerHTML="star_border";
        starFive.innerHTML="star_border";
    }
    else if(!starThreeClick && !starFourClick && !starFiveClick){
        starThree.innerHTML="star_border";
        starFour.innerHTML="star_border";
        starFive.innerHTML="star_border";
    }
    else if(!starFourClick && !starFiveClick){
        starFour.innerHTML="star_border";
        starFive.innerHTML="star_border";
    }
    else if(!starFiveClick){
        starFive.innerHTML="star_border";
    }
    
});
starFive.addEventListener('click', () => {
    starOneClick = false;
    starTwoClick = false;
    starThreeClick = false;
    starFourClick = false;
    starFiveClick = true;
    starOne.innerHTML="star";
    starTwo.innerHTML="star";
    starThree.innerHTML="star";
    starFour.innerHTML="star";
    starFive.innerHTML="star";
});


let checkBoxContacter = document.getElementById('checkBoxContacter'),
    checkBoxRelance = document.getElementById('checkBoxRelance'),
    checkBoxContacterClick = false,
    checkBoxRelanceClick = false;

    checkBoxContacter.addEventListener('click', () => {
        if(!checkBoxContacterClick){
            checkBoxContacter.innerHTML="check_box";
            checkBoxContacterClick = true;
        }
        else{
            checkBoxContacter.innerHTML="check_box_outline_blank";
            checkBoxContacterClick = false;
        }
        
    });

    checkBoxRelance.addEventListener('click' , () => {
        if(!checkBoxRelanceClick){
            checkBoxRelance.innerHTML="check_box";
            checkBoxRelanceClick = true;
        }
        else{
            checkBoxRelance.innerHTML="check_box_outline_blank";
            checkBoxRelanceClick = false;
        }
    });

let buttonSend = document.getElementById('buttonSend'),
    widgetImportance = document.getElementById('widgetImportance'),
    widgetContacter = document.getElementById('widgetContacter'),
    widgetRelancer = document.getElementById('widgetRelancer'),
    load = false,
    loadTwo

    document.addEventListener('DOMContentLoaded', () => {
        if(!load){
            if(widgetImportance.value == '1'){
                
                starOneClick = true;
                starOne.innerHTML="star";
            }
            else if (widgetImportance.value == '2'){
                load = true;
                starTwoClick = true;
                starOne.innerHTML="star";
                starTwo.innerHTML="star";
            }
            else if (widgetImportance.value == '3'){
                load = true;
                starThreeClick = true;
                starOne.innerHTML="star";
                starTwo.innerHTML="star";
                starThree.innerHTML="star";
            }
            else if (widgetImportance.value == '4'){
                load = true;
                starFourClick = true;
                starOne.innerHTML="star";
                starTwo.innerHTML="star";
                starThree.innerHTML="star";
                starFour.innerHTML="star";
            }
            else if (widgetImportance.value == '5'){
                load = true;
                starFiveClick = true;
                starOne.innerHTML="star";
                starTwo.innerHTML="star";
                starThree.innerHTML="star";
                starFour.innerHTML="star";
                starFive.innerHTML="star";
            }


            if(widgetContacter.value){
                checkBoxContacter.innerHTML="check_box";
                checkBoxContacterClick = true;
            }

            if(widgetRelancer.value){
                checkBoxRelance.innerHTML="check_box";
                checkBoxRelanceClick = true;
            }

            load = true;

        }
    })
    
buttonSend.addEventListener('click', () => {
    if(starOneClick){
        widgetImportance.value = '1';
    }
    else if(starTwoClick){
        widgetImportance.value = '2';
    }
    else if(starThreeClick){
        widgetImportance.value = '3';
    }
    else if(starFourClick){
        widgetImportance.value = '4';
    }
    else if(starFiveClick){
        widgetImportance.value = '5';
    }
    else{
        widgetImportance.value = '0';
    }

    if(checkBoxContacterClick){
        widgetContacter.value=1;
    }
    else{
        widgetContacter.value=0;
    }

    if(checkBoxRelanceClick){
        widgetRelancer.value=1;
    }
    else{
        widgetRelancer.value=0;
    }
    
})


