let nav = document.getElementById("navigation"),
            corp = document.getElementById("corp"),
            navWidth = nav.offsetWidth;
            console.log(navWidth)
            corp.style.marginLeft = navWidth+"px"
            corp.style.marginRight = navWidth+"px"
            corp.style.width = (corp.offsetWidth - navWidth)+ "px";

let cartes = document.getElementsByClassName("carte"),
    sousTitres = document.getElementsByClassName("sous-titre"),
    fonds = document.getElementsByClassName("fond");

    console.log(cartes);
    console.log(sousTitres);
    console.log(fonds)

    for (let i = 0; i < cartes.length; i++) {
        cartes[i].addEventListener("mouseover", () => {
            cartes[i].className = "carte container-fluid bg-white shadow-lg zindex-popover p-2 mr-3 border"; 
        });
        cartes[i].addEventListener("mouseout", () => {
            cartes[i].className="carte container-fluid bg-white shadow-sm p-2 mr-3 border";
        })
    }

    for(let i = 0; i < fonds.length; i++){
        fonds[i].addEventListener('mouseover' , () => {
            sousTitres[i].className = "border-top border border-primary w-75 mt-1 mb-3 sous-titre";
        })

        fonds[i].addEventListener('mouseout', () => {
            sousTitres[i].className = "border-top border w-75 mt-1 mb-3 sous-titre";
        })
    }
