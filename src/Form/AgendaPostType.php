<?php

namespace App\Form;

use App\Entity\AgendaPost;
use App\Entity\Candidature;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgendaPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('candidature', EntityType::class , 
            [
                'class' => Candidature::class,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('s')
                    ->select('s');
                }
                
            ]
            )
            ->add('intituler')
            ->add('dateLimite', DateTimeType::class)
            ->add('fait')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AgendaPost::class,
        ]);
    }
}
