<?php

namespace App\Form;

use App\Entity\Candidature;
use App\Entity\DetailCandidature;
use App\Entity\Document;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Webmozart\Assert\Assert;

class DocumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('document', FileType::class, [
                'label' => 'Document (PDF)',
                'mapped' => false,
                
                
            ]
            )
            ->add('candidature', EntityType::class, [
                'class' => Candidature::class,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('c');
                },
                'choice_label' => 'detailCandidature.poste'

            ]);
            
            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
            
        ]);
    }
}
