<?php

namespace App\Repository;

use App\Entity\AgendaPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AgendaPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgendaPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgendaPost[]    findAll()
 * @method AgendaPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method AgendaPost[]    findAllByFirstDate()
 */
class AgendaPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgendaPost::class);
    }

    public function findAllByFirstDate(){
        return $this->createQueryBuilder('a')
            ->orderBy('a.date_limite','ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return AgendaPost[] Returns an array of AgendaPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgendaPost
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
