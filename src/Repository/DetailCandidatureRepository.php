<?php

namespace App\Repository;

use App\Entity\DetailCandidature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DetailCandidature|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailCandidature|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailCandidature[]    findAll()
 * @method DetailCandidature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailCandidatureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetailCandidature::class);
    }

    // /**
    //  * @return DetailCandidature[] Returns an array of DetailCandidature objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetailCandidature
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
