<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AgendaPostRepository")
 */
class AgendaPost
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agenda", cascade={"persist", "remove"})
     */
    private $agenda;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intituler;

    /**
     * @ORM\Column(type="boolean")
     */
    private $fait;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_limite;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidature", cascade={"persist", "remove"})
     */
    private $candidature;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $update_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgenda(): ?Agenda
    {
        return $this->agenda;
    }

    public function setAgenda(?Agenda $agenda): self
    {
        $this->agenda = $agenda;

        return $this;
    }

    public function getIntituler(): ?string
    {
        return $this->intituler;
    }

    public function setIntituler(string $intituler): self
    {
        $this->intituler = $intituler;

        return $this;
    }

    public function getDateLimite() : ?\DateTimeInterface
    {
        return $this->date_limite;
    }

    public function setDateLimite(\DateTimeInterface $date_limite) : self
    {
        $this->date_limite = $date_limite;
        return $this;
    }
    public function getFait(): ?bool
    {
        return $this->fait;
    }

    public function setFait(bool $fait): self
    {
        $this->fait = $fait;

        return $this;
    }

    public function getCandidature(): ?Candidature
    {
        return $this->candidature;
    }

    public function setCandidature(?Candidature $candidature): self
    {
        $this->candidature = $candidature;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->create_at;
    }

    public function setCreateAt(\DateTimeInterface $create_at): self
    {
        $this->create_at = $create_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->update_at;
    }

    public function setUpdateAt(\DateTimeInterface $update_at): self
    {
        $this->update_at = $update_at;

        return $this;
    }
}
