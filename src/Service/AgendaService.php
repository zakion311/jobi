<?php
namespace App\Service;

use AgendaException;
use App\Entity\AgendaPost;
use App\Repository\AgendaPostRepository;
use DateTime;
use Doctrine\Persistence\ObjectManager;

class AgendaService 
{
    private $ERROR_INTITULER_LEN = "Votre intituler doit contenir entre 3 est 50 caractère";
    private $ERROR_DATE_BEFORE = "Cette date est entérieure a notre date";
    private $ERROR_DATE_SAME = "Cette date est deja occuper";
    
    public function check(AgendaPost $agendaPost, AgendaPostRepository $agendaPostRepository): ?AgendaPost{
        $agendaException = new AgendaException();
        $now = new DateTime("now");
        $error = false;

        if(strlen($agendaPost->getIntituler()) < 3 || strlen($agendaPost->getIntituler()) > 50){
            $error = true;
            $agendaException->setErrorIntituler($this->ERROR_INTITULER_LEN);
        }
        
        if($agendaPost->getDateLimite() < $now){
            $error = true;
            $agendaException->setErrorDateLimite($this->ERROR_DATE_BEFORE);
        }

        if($this->sameDate($agendaPost, $agendaPostRepository)){
            $error = true;
            $agendaException->setErrorDateLimite($this->ERROR_DATE_SAME);
        }

        if($error){
            throw $agendaException;
        }

        $date = new DateTime();
        $date ->format("Y-m-d H:i:s");
        $agendaPost->setCreateAt($date);
        $agendaPost->setUpdateAt($date);

        return $agendaPost;


    }

    public function save(AgendaPost $agendaPost, ObjectManager $entityManager){
        $entityManager->persist($agendaPost);
        $entityManager->flush();
    }

    public function sameDate(AgendaPost $agendaPost, AgendaPostRepository $agendaPostRepository): ?bool{
        $agendaPosts = $agendaPostRepository->findAll();
        foreach ($agendaPosts as $key => $value) {
            if($agendaPost->getId() != $value->getId()){
                if($agendaPost->getDateLimite() == $value->getDateLimite()){
                    return true;
                }
            }
        }
        return false;
    }
}


?>