<?php 
namespace App\Service;

use App\Entity\Candidature;
use App\Repository\CandidatureRepository;
use CandidatureException;
use DateTime;
use Doctrine\Persistence\ObjectManager;

class CandidatureService 
{
    private $ERROR_IMPORTANCE = "L'importance doit ce trouver entre 0 et 5.";
    private $ERROR_POSTE_LEN = "Le poste doit contenir entre 3 et 20 caractère.";
    private $ERROR_POSTE_SAME = "Ce poste est déja présent dans une autre candidature de cette société";
    private $ERROR_NOTE = "Votre note ne doit pas contenir plus de 255 caractère";

    public function check(Candidature $candidature, CandidatureRepository $candidatureRepository){
        $candidatureException = new CandidatureException();
        $candidatureRepository = $candidatureRepository;
        $error = false;
        $detailCandidature = $candidature->getDetailCandidature();

        if($detailCandidature->getImportance() < 0 || $detailCandidature->getImportance() > 5 ){
            $error = true;
            $candidatureException->setErrorImportance($this->ERROR_IMPORTANCE);
        }

        if(strlen($detailCandidature->getPoste()) < 3 || strlen($detailCandidature->getPoste()) > 50){
            $error = true;
            $candidatureException->setErrorPoste($this->ERROR_POSTE_LEN);
        }
        
        if($this->samePoste($candidature, $candidatureRepository)){
            $error = true;
            $candidatureException->setErrorPoste($this->ERROR_POSTE_SAME);
        }

        if(strlen($detailCandidature) > 255){
            $error = true;
            $candidatureException->setErrorNote($this->ERROR_NOTE);
        }

        if($error){
            throw $candidatureException;
        }

        $date = new DateTime();
        $date ->format("Y-m-d H:i:s");
        $candidature->getDetailCandidature()->setCreateAt($date);
        $candidature->getDetailCandidature()->setUpdateAt($date);
        $candidature->setCreateAt($date);
        $candidature->setUpdateAt($date);
        $candidature->setActif(true);
        
        return $candidature;

    }
    
    public function save(Candidature $candidature, ObjectManager $entityManager){
        $entityManager->persist($candidature);
        $entityManager->flush();
    }

    public function samePoste(Candidature $candidature, CandidatureRepository $candidatureRepository) : ?bool{
        $candidatures = $candidatureRepository->findAll();
        foreach ($candidatures as $key => $value) {
            if($candidature->getSociete()->getNom() == $value->getSociete()->getNom()){
                if($candidature->getDetailCandidature()->getPoste() == $value->getDetailCandidature()->getPoste()){
                    if($candidature->getId() != $value->getId()){
                        return true;
                    }
                    
                }
            }
        }
        return false;

    }
}


?>