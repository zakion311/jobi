<?php
namespace App\Service;

use App\Controller\DocumentController;
use App\Entity\Document;
use App\Repository\DocumentRepository;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use DocumentException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class DocumentService 
{   
    private $ERROR_SIZE = "La taille de votre fichier dépasse 4MB";
    private $ERROR_NAME_SAME = "Ce fichier est déja présent pour cette candidature";
    private $ERROR_NAME_FORMAT = "Le nom de votre fichier ne doit pas contenire de point.";
    private $ERROR_EXTENSION = "L'extension de votre fichier n'est pas un pdf";

    public function check(Document $document,$documentFile,  DocumentRepository $documentRepository, string $originaleFileName){
        $documentException = new DocumentException();
        $error = false;
        
        if(!$this->goodName($documentFile->getClientOriginalName())){
            $error = true;
            $documentException->setErrorFormat($this->ERROR_NAME_FORMAT);
        }
        
        if($documentFile->getSize() > 4000000){
            $error = true;
            $documentException->setErrorSize($this->ERROR_SIZE);
        }

        if($this->sameName($documentFile, $documentRepository,$document)){
            $error = true;
            $documentException->setErrorName($this->ERROR_NAME_SAME);
        }
        
        if($documentFile->guessExtension() != 'pdf'){
            $error = true;
            $documentException->setErrorExtension($this->ERROR_EXTENSION);
            
        }

        if($error){
            throw $documentException;
        }

        $newFileName = $this->generateUniqueName($originaleFileName, $documentFile);

        $document->setSize($documentFile->getSize());
        $date = new DateTime();
        $date ->format("Y-m-d H:i:s");
        $document->setOriginalName($originaleFileName);
        $document->setName($newFileName);
        $document->setCreateAt($date);
        $document->setUpdateAt($date);

        return $document;
        
    }

    public function save(Document $document, ObjectManager $entityManager){
        $entityManager->persist($document);
        $entityManager->flush();
    }

    public function readFile(Document $document, string $dir){
        $fileName = $dir."/".$document->getName();
        header("Content-type:application/pdf");
        header('Content-Disposition: inline; filename="' . $fileName . '"'); 
        header('Content-Transfer-Encoding: binary'); 
        header('Accept-Ranges: bytes'); 
        readfile($fileName);
    }
    public function upload($documentFile, string $dir, Document $document){
        try{
            $documentFile->move($dir,$document->getName());
        }catch(FileException $e){
            die('error');
        }
    }

    public function generateUniqueName(string $originaleFileName, $documentFile): ?string{
        $safeFileName = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',$originaleFileName); 
        return $safeFileName .'-'.uniqid().'.'.$documentFile->guessExtension();
    }


    public function convert_filesize($bytes, $decimals = 2){
        $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

    public function sameName($documentFile, DocumentRepository $documentRepository, Document $document): ?bool{
        $documents = $documentRepository->findAll();
        $originalName = pathinfo($documentFile->getClientOriginalName(), PATHINFO_FILENAME);
        foreach ($documents as $key => $value) {
            if($document->getId() != $value->getId()){
                if($document->getCandidature()->getId() == $value->getCandidature()->getId()){
                    if($originalName == $value->getOriginalName()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function goodName(string $name){
        if(count(explode(".",$name)) > 2){
            return false;
        }
        return true;

    }
    
}




?>