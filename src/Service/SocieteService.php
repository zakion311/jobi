<?php 
namespace App\Service;

use App\Entity\Societe;
use App\Repository\SocieteRepository;
use DateTime;
use Doctrine\Persistence\ObjectManager;
use SocieteException;

class SocieteService 
{   
    private $ERROR_NOM_LEN = "Votre nom doit contenire entre 3 et 20 caractère.";
    private $ERROR_NOM_SAME = "Cette société est déja utilisé.";
    private $ERROR_MAIL = "Votre mail n'est pas valide";
    private $ERROR_TELEPHONE = "Votre numéro n'est pas valide, il ne doit pas contenir d'espace";
    private $ERROR_NOTE = "Votre note ne doit pas contenir plus de 255 caractère";

    function check(Societe $societe, SocieteRepository $societeRepository) : ?Societe {
        $societeRepository = $societeRepository;
        $societeException = new SocieteException();
        $error = false;

        if(strlen($societe->getNom()) < 3 || strlen($societe->getNom()) > 20 ){
            $error = true;
            $societeException->setErrorNom($this->ERROR_NOM_LEN);
        }
        if($this->sameSociete($societe,$societeRepository)){
            $error = true;
            $societeException->setErrorNom($this->ERROR_NOM_SAME);
        }
        
        if(!preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#i",$societe->getMail())){
            $error = true;
            $societeException->setErrorMail($this->ERROR_MAIL);
        }

        if(strlen($societe->getTelephone()) != 10){
            $error = true;
            $societeException->setErrorTelephone($this->ERROR_TELEPHONE);
        }

        if(strlen($societe->getNote()) > 255 ){
            $error = true;
            $societeException->setErrorNote($this->ERROR_NOTE);
        }

        if($error){
            throw $societeException;
            
        }
        else{

            $date = new DateTime();
            $date ->format("Y-m-d H:i:s");
            $societe->setCreateAt($date);
            $societe->setUpdateAt($date);

            return $societe;
        }
    }

    function sameSociete(Societe $societe, SocieteRepository $societeRepository) : ?bool{
        $societes = $societeRepository->findAll();
        foreach ($societes as $key => $value) {
            if($societe->getNom() == $value->getNom()){
                if($societe->getId() != $value->getNom()){
                    return true;
                }
            }
        }
        return false;
    }

    function edit(){

    }

    function save(Societe $societe,ObjectManager $entityManager){
        $entityManager->persist($societe);
        $entityManager->flush();
    }
    
}




?>