<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200327121048 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidature DROP INDEX UNIQ_E33BD3B8FCF77503, ADD INDEX IDX_E33BD3B8FCF77503 (societe_id)');
        $this->addSql('ALTER TABLE agenda_post RENAME INDEX uniq_67afcefdea67784a TO IDX_67AFCEFDEA67784A');
        $this->addSql('ALTER TABLE agenda_post RENAME INDEX uniq_67afcefdb6121583 TO IDX_67AFCEFDB6121583');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE agenda_post RENAME INDEX idx_67afcefdb6121583 TO UNIQ_67AFCEFDB6121583');
        $this->addSql('ALTER TABLE agenda_post RENAME INDEX idx_67afcefdea67784a TO UNIQ_67AFCEFDEA67784A');
        $this->addSql('ALTER TABLE candidature DROP INDEX IDX_E33BD3B8FCF77503, ADD UNIQUE INDEX UNIQ_E33BD3B8FCF77503 (societe_id)');
    }
}
