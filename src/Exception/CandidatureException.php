<?php 
class CandidatureException extends Exception 
{
    private $errorImportance = "";
    private $errorPoste = "";
    private $errorContacter = "";
    private $errorRelancer = "";
    private $errorNote = "";


    public function getErrorImportance(): ?string {
        return $this->errorImportance;
    }

    public function setErrorImportance(string $errorImportance){
        $this->errorImportance = $errorImportance;
    }

    public function getErrorPoste(): ?string{
        return $this->errorPoste;
    }

    public function setErrorPoste(string $errorPoste){
        $this->errorPoste = $errorPoste;
    }

    public function getErrorContacter() : ?string{
        return $this->errorContacter;
    }

    public function setErrorContacter(string $errorContacter){
        $this->errorContacter = $errorContacter;
    }

    public function getErrorRelancer(): ?string{
        return $this->errorRelancer;
    }

    public function setErrorRelancer(string $errorRelancer){
        $this->errorRelancer = $errorRelancer;
    }

    public function getErrorNote() : ?string {
        return $this->errorNote;
    }

    public function setErrorNote(string $errorNote){
        $this->errorNote = $errorNote;
    }

}


?>