<?php 

class SocieteException extends Exception
{
    private $errorNom = "";

    private $errorMail = "";

    private $errorTelephone = "";

    private $errorNote = "";

    public function getErrorNom() : ?string {
        return $this->errorNom;
    }

    public function setErrorNom(string $errorNom){
        $this->errorNom = $errorNom;
    }

    public function getErrorMail() : ?string {
        return $this->errorMail;
    }

    public function setErrorMail(string $errorMail){
        $this->errorMail = $errorMail;
    }

    public function getErrorTelephone() : ?string{
        return $this->errorTelephone;
    }

    public function setErrorTelephone(string $errorTelephone){
        $this->errorTelephone = $errorTelephone;
    }

    public function getErrorNote(){
        return $this->errorNote;
    }

    public function setErrorNote(string $errorNote){
        $this->errorNote = $errorNote;
    }
}



?>