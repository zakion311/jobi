<?php 
class DocumentException extends Exception 
{
    private $errorName;
    private $errorFormat;
    private $errorSize;
    private $errorExtension;

    public function getErrorName() : ?string {
        return $this->errorName;
    }

    public function setErrorName(string $errorName){
        $this->errorName = $errorName;
    }

    public function getErrorFormat(): ?string{
        return $this->errorFormat;
    }

    public function setErrorFormat(string $errorFormat){
        $this->errorFormat = $errorFormat;
    }

    public function getErrorSize() : ?string{
        return $this->errorSize;
    
    }

    public function setErrorSize(string $errorSize){
        $this->errorSize = $errorSize;
    }

    public function getErrorExtension(): ?string{
        return $this->errorExtension;
    }

    public function setErrorExtension(string $errorExtension){
        $this->errorExtension = $errorExtension;
    }

}
?>