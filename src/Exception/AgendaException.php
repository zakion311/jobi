<?php 
class AgendaException extends Exception 
{
    private $errorIntituler = "" ;
    private $errorFait = "";
    private $errorDateLimite = "";
    

    public function getErrorIntituler() : ?string{
        return $this->errorIntituler;
    }

    public function setErrorIntituler(string $errorIntituler){
        $this->errorIntituler = $errorIntituler;
    }

    public function getErrorFait() : ?string {
        return $this->errorFait;
    }

    public function setErrorFait(string $errorFait){
        $this->errorFait = $errorFait;
    }

    public function getErrorDateLimite() : ?string {
        return $this->errorDateLimite;
    }

    public function setErrorDateLimite(string $errorDateLimite){
        $this->errorDateLimite = $errorDateLimite;
    }
}


?>