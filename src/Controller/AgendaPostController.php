<?php

namespace App\Controller;

use AgendaException;
use App\Entity\Agenda;
use App\Entity\AgendaPost;
use App\Form\AgendaPostType;
use App\Repository\AgendaPostRepository;
use App\Repository\AgendaRepository;
use App\Service\AgendaService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/agenda")
 */
class AgendaPostController extends AbstractController
{
    /**
     * @Route("/", name="agenda_post_index", methods={"GET"})
     */
    public function index(AgendaPostRepository $agendaPostRepository): Response
    {
        $agendaPosts = $agendaPostRepository->findAll();
        $candidatures = [];
        $detailCandidatures = [];
        $societes = [];
        foreach ($agendaPosts as $key => $value) {
            array_push($candidatures,$value->getCandidature());
            array_push($detailCandidatures,$candidatures[$key]->getDetailCandidature());
            array_push($societes,$candidatures[$key]->getSociete());
        } 
        return $this->render('agenda_post/index.html.twig', [
            'agenda_posts' => $agendaPostRepository->findAll(),
            'candidatures' => $candidatures,
            'societes' => $societes,
            'index' => count($agendaPosts)
        ]);
    }

    /**
     * @Route("/new", name="agenda_post_new", methods={"GET","POST"})
     */
    public function new(Request $request, AgendaRepository $agendaRepository, AgendaPostRepository $agendaPostRepository, AgendaService $agendaService): Response
    {
        $agendaPost = new AgendaPost();
        $form = $this->createForm(AgendaPostType::class, $agendaPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $entityManager = $this->getDoctrine()->getManager();

                $agenda = $agendaRepository->find(1);
                $agendaPost->setAgenda($agenda);

                $agendaPost = $agendaService->check($agendaPost,$agendaPostRepository);
                $agendaService->save($agendaPost, $entityManager);

                return $this->redirectToRoute('agenda_post_index');
            }catch(AgendaException $agendaException){
                return $this->render('agenda_post/new.html.twig', [
                    'agenda_post' => $agendaPost,
                    'form' => $form->createView(),
                    'agendaPostError' => $agendaException
                ]);
            }
        }

        return $this->render('agenda_post/new.html.twig', [
            'agenda_post' => $agendaPost,
            'form' => $form->createView(),
            'agendaPostError' => new AgendaException()
        ]);
    }

    /**
     * @Route("/{id}", name="agenda_post_show", methods={"GET"})
     */
    public function show(AgendaPost $agendaPost): Response
    {   
        $agendaPost = $agendaPost;
        $candidature = $agendaPost->getCandidature();
        $detailCandidature = $candidature->getDetailCandidature();
        $societe = $candidature->getSociete();
        
        return $this->render('agenda_post/show.html.twig', [
            'agenda_post' => $agendaPost,
            'detailCandidature' => $detailCandidature,
            'societe' => $societe,
            'candidature' => $candidature
        ]);
    }

    /**
     * @Route("/{id}/edit", name="agenda_post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AgendaPost $agendaPost, AgendaPostRepository $agendaPostRepository, AgendaService $agendaService): Response
    {
        $form = $this->createForm(AgendaPostType::class, $agendaPost);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $agendaPost = $agendaService->check($agendaPost,$agendaPostRepository);
                $agendaService->save($agendaPost, $entityManager);
                return $this->redirectToRoute('agenda_post_index');
            }catch(AgendaException $agendaException){
                return $this->render('agenda_post/edit.html.twig', [
                    'agenda_post' => $agendaPost,
                    'form' => $form->createView(),
                    'agendaPostError' => $agendaException
                ]);
            }
        }

        return $this->render('agenda_post/edit.html.twig', [
            'agenda_post' => $agendaPost,
            'form' => $form->createView(),
            'agendaPostError' => new AgendaException()
        ]);
    }

    /**
     * @Route("/{id}", name="agenda_post_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AgendaPost $agendaPost): Response
    {
        if ($this->isCsrfTokenValid('delete'.$agendaPost->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($agendaPost);
            $entityManager->flush();
        }

        return $this->redirectToRoute('agenda_post_index');
    }
}
