<?php

namespace App\Controller;

use App\Entity\Document;
use App\Form\DocumentType;
use App\Repository\CandidatureRepository;
use App\Repository\DocumentRepository;
use App\Service\DocumentService;
use DateTime;
use DocumentException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/document")
 */
class DocumentController extends AbstractController
{
    /**
     * @Route("/", name="document_index")
     */
    public function index(DocumentRepository $documentRepository, DocumentService $documentService)
    {
        $documentNames = [];
        $documentSizes = [];
        $documents = $documentRepository->findAll();
        foreach ($documents as $key => $document) {
            $name = $document->getOriginalName();
            array_push($documentNames, $name);
            $size = $documentService->convert_filesize($document->getSize());
            array_push($documentSizes, $size);
        }
        $index = $documents;
        return $this->render('document/index.html.twig', [
            'documents' => $documents,
            'documentNames' => $documentNames,
            'documentSizes' => $documentSizes,
            'index' => count($documents),
        ]);
    }

    /**
     * @Route("/{id}", name="document_show", methods={"GET"})
     */
    public function show(Document $document, DocumentRepository $documentRepository, DocumentService $documentService){
        $candidature = $document->getCandidature();
        $detailCandidature = $candidature->getDetailCandidature();
        $societe = $candidature->getSociete();

        return $this->render('document/show.html.twig', [
            'document' => $document,
            'candidature' => $candidature,
            'size' => $documentService->convert_filesize($document->getSize()),
            'detailCandidature' => $detailCandidature,
            'societe' => $societe
        ]);
    }

    /**
     * @Route("/read/{id}", name="document_read", methods={"GET"})
     */
    public function read(Document $document, DocumentService $documentSerivce){
        $dir = $this->getParameter('document_directory');
        $documentSerivce->readFile($document,$dir);
    }

    /**
     * @Route("/new", name="document_new", methods={"GET","POST"})
     */
    public function new(Request $request, CandidatureRepository $candidatureRepository, DocumentService $documentService, DocumentRepository $documentRepository):Response{
        $document = new Document();
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $documentFile = $form->get('document')->getdata();
                $originaleFileName = pathinfo($documentFile->getClientOriginalName(), PATHINFO_FILENAME);
            
                $document = $documentService->check($document, $documentFile, $documentRepository,$originaleFileName);
                $dir = $this->getParameter('document_directory');
                $documentService->upload($documentFile,$dir,$document);
                $documentService->save($document,$entityManager);

                return $this->redirectToRoute('document_index');
            }catch(DocumentException $documentException){
                return $this->render('document/new.html.twig', [
                    'form' => $form->createView(),
                    'documentError' => $documentException
                    
                ]);
            }

        }

        return $this->render('document/new.html.twig', [
            'form' => $form->createView(),
            'documentError' => new DocumentException()
            
        ]);
    }
}
