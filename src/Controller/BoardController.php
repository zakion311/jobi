<?php

namespace App\Controller;

use App\Entity\AgendaPost;
use App\Repository\AgendaPostRepository;
use App\Repository\CandidatureRepository;
use App\Repository\SocieteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BoardController extends AbstractController
{
    /**
     * @Route("/board", name="board")
     */
    public function index(AgendaPostRepository $agendaPostRepository, SocieteRepository $societeRepository, CandidatureRepository $candidatureRepository)
    {
        
        $agendaPosts = $agendaPostRepository->findAllByFirstDate();
        $societes = $societeRepository->findAllByFirstDate();
        $candidatures = $candidatureRepository->findAllByFirstDate();
        $detailCandidatures = [];
        $societesCandidature = [];
        $societesAgenda = [];
        foreach ($candidatures as $key => $value) {
            array_push($detailCandidatures,$value->getDetailCandidature());
            array_push($societesCandidature, $value->getSociete());
        }

        foreach($agendaPosts as $key => $value){
            array_push($societesAgenda, $value->getCandidature()->getSociete());
        }
        
        return $this->render('board/index.html.twig', [
            'agenda_posts' => $agendaPosts,
            'societes' => $societes,
            'candidatures' => $candidatures,
            'detailCandidatures' => $detailCandidatures,
            'societesCandidature' => $societesCandidature,
            'societesAgenda' => $societesAgenda,
            'indexAgenda' => count($agendaPosts),
            'indexCandidature' => count($candidatures)
        ]);
    }
}
