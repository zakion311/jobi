<?php

namespace App\Controller;

use App\Entity\Candidature;
use App\Entity\DetailCandidature;
use App\Form\CandidatureType;
use App\Repository\CandidatureRepository;
use App\Service\CandidatureService;
use CandidatureException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Datetime;
/**
 * @Route("/candidature")
 */
class CandidatureController extends AbstractController
{
    /**
     * @Route("/", name="candidature_index", methods={"GET"})
     */
    public function index(CandidatureRepository $candidatureRepository): Response
    {
        $candidatures = $candidatureRepository->findAll();
        $detailCandidatures = [];
        $societes = [];
        foreach ($candidatures as $candidature) {
            array_push($detailCandidatures,$candidature->getDetailCandidature());
            array_push($societes,$candidature->getSociete());
        }
        return $this->render('candidature/index.html.twig', [
            'candidatures' => $candidatureRepository->findAll(),
            'detailCandidatures' => $detailCandidatures,
            'societes' => $societes,
            'index' => count($detailCandidatures)
        ]);
    }

    /**
     * @Route("/new", name="candidature_new", methods={"GET","POST"})
     */
    public function new(Request $request, CandidatureRepository $candidatureRepository, CandidatureService $candidatureService): Response
    {
        $candidature = new Candidature();
        $form = $this->createForm(CandidatureType::class, $candidature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $candidature = $candidatureService->check($candidature, $candidatureRepository);
                $candidatureService->save($candidature, $entityManager);

                return $this->redirectToRoute('candidature_index');
            }catch(CandidatureException $candidatureException){
                return $this->render('candidature/new.html.twig', [
                    'candidature' => $candidature,
                    'form' => $form->createView(),
                    'candidatureError' => $candidatureException
                ]);
            }
        }

        return $this->render('candidature/new.html.twig', [
            'candidature' => $candidature,
            'form' => $form->createView(),
            'candidatureError' => new CandidatureException()
        ]);
    }

    /**
     * @Route("/{id}", name="candidature_show", methods={"GET"})
     */
    public function show(Candidature $candidature): Response
    {   
        $detailCandidature = $candidature->getDetailCandidature();
        $societe = $candidature->getSociete();
        return $this->render('candidature/show.html.twig', [
            'candidature' => $candidature,
            'detailCandidature' => $detailCandidature,
            'societe' => $societe
        ]);
    }

    /**
     * @Route("/{id}/edit", name="candidature_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Candidature $candidature,CandidatureRepository $candidatureRepository, CandidatureService $candidatureService): Response
    {
        $form = $this->createForm(CandidatureType::class, $candidature);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $candidature = $candidatureService->check($candidature,$candidatureRepository);
                $candidatureService->save($candidature,$entityManager);
                return $this->redirectToRoute('candidature_index');
            }catch(CandidatureException $candidatureException){
                return $this->render('candidature/edit.html.twig', [
                    'candidature' => $candidature,
                    'form' => $form->createView(),
                    'candidatureError' => $candidatureException
                ]);
            }

           
        }

        return $this->render('candidature/edit.html.twig', [
            'candidature' => $candidature,
            'form' => $form->createView(),
            'candidatureError' => new CandidatureException()
        ]);
    }

    /**
     * @Route("/{id}", name="candidature_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Candidature $candidature): Response
    {
        if ($this->isCsrfTokenValid('delete'.$candidature->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($candidature);
            $entityManager->flush();
        }

        return $this->redirectToRoute('candidature_index');
    }
}
