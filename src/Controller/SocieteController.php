<?php

namespace App\Controller;

use App\Entity\Societe;
use App\Form\SocieteType;
use App\Repository\SocieteRepository;
use App\Service\SocieteService;
use DateTime;
use SocieteException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/societe")
 */
class SocieteController extends AbstractController
{
    /**
     * @Route("/", name="societe_index", methods={"GET"})
     */
    public function index(SocieteRepository $societeRepository): Response
    {
        return $this->render('societe/index.html.twig', [
            'societes' => $societeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="societe_new", methods={"GET","POST"})
     */
    public function new(Request $request, SocieteService $societeService, SocieteRepository $societeRepository): Response
    {
        $societe = new Societe();
        $form = $this->createForm(SocieteType::class, $societe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $societe = $societeService->check($societe, $societeRepository);
                $societeService->save($societe, $entityManager);

                return $this->redirectToRoute('societe_index');
            }
            catch(SocieteException $societeException){
                return $this->render('societe/new.html.twig', [
                    'societe' => $societe,
                    'form' => $form->createView(),
                    'societeError' => $societeException
                ]);
            }
            
        }

        return $this->render('societe/new.html.twig', [
            'societe' => $societe,
            'form' => $form->createView(),
            'societeError' => new SocieteException(),
        ]);
    }

    /**
     * @Route("/{id}", name="societe_show", methods={"GET"})
     */
    public function show(Societe $societe): Response
    {
        return $this->render('societe/show.html.twig', [
            'societe' => $societe,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="societe_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Societe $societe, SocieteService $societeService, SocieteRepository $societeRepository): Response
    {
        $form = $this->createForm(SocieteType::class, $societe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $societe = $societeService->check($societe, $societeRepository);
                $societeService->save($societe, $entityManager);

                return $this->redirectToRoute('societe_index');

            }catch(SocieteException $societeException){
                return $this->render('societe/edit.html.twig', [
                    'societe' => $societe,
                    'form' => $form->createView(),
                    'societeError' => $societeException
                ]);
            }

            
        }

        return $this->render('societe/edit.html.twig', [
            'societe' => $societe,
            'form' => $form->createView(),
            'societeError' => new SocieteException()
        ]);
    }

    /**
     * @Route("/{id}", name="societe_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Societe $societe): Response
    {
        if ($this->isCsrfTokenValid('delete'.$societe->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($societe);
            $entityManager->flush();
        }

        return $this->redirectToRoute('societe_index');
    }
}
